﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;

namespace Peliculas.Services.Interfaces
{
    public interface IActorServices : IGenericServices<Actor>
    {
        Task<List<PeliculaActorDTO>> GetByName(string Nombre);

    }
}
