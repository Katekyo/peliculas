﻿using System;
using System.Threading.Tasks;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Entries.Helpers;
using Peliculas.Services.Interfaces;

namespace Peliculas.Services
{
    public class PeliculaServices : GenericService<Movie>, IPeliculaServices
    {
        private IPeliculaRepository _peliculaRepository;
        public PeliculaServices(IPeliculaRepository peliculaRepostory) : base(peliculaRepostory)
        {
            _peliculaRepository = peliculaRepostory;
        }

        public void EscribirOrdenActores(Movie pelicula)
        {
            if (pelicula.PeliculasActores != null)
            {
                for (int i = 0; i < pelicula.PeliculasActores.Count; i++)
                {
                    pelicula.PeliculasActores[i].Orden = i;
                }
            }
        }

        public async Task<EstrenoEncine> GetAllMovies()
        {
            return await _peliculaRepository.GetAllMovies();
        }

        public async Task<Movie> GetPeliculaById(int id)
        {
            return await _peliculaRepository.GetPeliculaById(id);
        }

        public async Task<Movie> GetPeliculaToUpdateById(int id)
        {
            return await _peliculaRepository.GetPeliculaById(id);
        }
    }
}
