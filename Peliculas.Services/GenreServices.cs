﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Services.Interfaces;

namespace Peliculas.Services
{
    public class GenreServices : GenericService<Genre>, IGenreServices
    {
        private IGenreRepository _genreRepository;
        public GenreServices(IGenreRepository genericRepostory) : base(genericRepostory)
        {
            _genreRepository = genericRepostory;
        }

        public async Task<List<Genre>> GetGeneroNoSeleccionado(List<int> generosSeleccionadosIds)
        {
            return await _genreRepository.GetGeneroNoSeleccionado(generosSeleccionadosIds);
        }
    }
}
