﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Peliculas.Entries
{
    public class Genre : EntityBaseClass
    {
        //public int Id { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 50, ErrorMessage = "El limite de caracteres del campo {0} debe ser de 50")]
        public string Nombre { get; set; }
        public List<PeliculasGeneros> PeliculasGeneros { get; set; }

    }
}
