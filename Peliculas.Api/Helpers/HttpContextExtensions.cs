﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Peliculas.Api.Helpers
{
    public static class HttpContextExtensions
    {
       public async static Task InsertParamsPageInHeader<T> (this HttpContext httpContext, IQueryable<T> queryable)
        {
            if (httpContext == null)
                throw new ArgumentException(nameof(httpContext));

            double quantity = await queryable.CountAsync();
            httpContext.Response.Headers.Add("entries", quantity.ToString());
        }
    }
}
