﻿using System;
using System.Linq;
using Peliculas.Entries.DTOs;

namespace Peliculas.Api.Helpers
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T>Paginetion<T>(this IQueryable<T> queryable, PaginationDTO paginationDTO)
        {
            return queryable.Skip((paginationDTO.Page - 1) * paginationDTO.RecordsPerPages).Take(paginationDTO.RecordsPerPages);
        }
    }
}
