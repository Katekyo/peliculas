﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Peliculas.Api.Helpers;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Services.Interfaces;


namespace Peliculas.Api.Controllers
{
    [Route("api/generos")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IGenreServices _genreServ;
        private readonly IMapper _mapper;

        public GenreController(IGenreServices genreServ, IMapper maper )
        {
            _genreServ = genreServ;
            _mapper = maper;
        }

        [HttpGet("todos")]
        public async Task<ActionResult<List<GenreDTO>>> Todos()
        {
            var generos = await _genreServ.GetAll().ToListAsync();
            return _mapper.Map<List<GenreDTO>>(generos);
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public async Task<ActionResult<List<GenreDTO>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable =  _genreServ.GetAll();
            await HttpContext.InsertParamsPageInHeader(queryable);
            var genre = await queryable.OrderBy(x => x.Nombre).Paginetion(paginationDTO).ToListAsync();
            return _mapper.Map<List<GenreDTO>>(genre); 
        }


        [HttpGet("{Id:int}")]
        public async Task<ActionResult<GenreDTO>> Get(int Id)
        {
            var genero = await _genreServ.GetById(Id);
            if (genero == null)
            {
                return NotFound();
            }
            return _mapper.Map<GenreDTO>(genero);
        }

        //Agregar
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] GenreCreateDTO genre)
        {
            await _genreServ.Add(_mapper.Map<Genre>(genre));
            return NoContent(); 
        }

        [HttpPut("{Id:int}")]
        public async Task<ActionResult> Put(int Id, [FromBody] GenreCreateDTO genreCreateDTO)
        {
            var genero = await _genreServ.GetById(Id);
            if (genero == null)
            {
                return NotFound();
            }
            genero = _mapper.Map(genreCreateDTO, genero);
            await _genreServ.Update(genero);
            return NoContent();
        }


        [HttpDelete("{Id:int}")]
        public async Task<ActionResult> Delete(int Id)
        {
            var existeGenero = await _genreServ.GetById(Id);
            if (existeGenero == null)
            {
                return NotFound();
            }
           await _genreServ.Remove(existeGenero.Id);

            return NoContent();
        }
    }
}
