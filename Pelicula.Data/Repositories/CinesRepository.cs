﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pelicula.Data.Data;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;

namespace Pelicula.Data.Repositories
{
    public class CinesRepository : GenericRepository<Cine>, ICineRepository
    {
        public CinesRepository(ApplicationDbContext context) : base(context)
        {
        }
        public async Task<List<Cine>> GetCineNoSeleccionado(List<int> cinesNoSeleccionados)
        {
            return await _context.Cines.Where(x => !cinesNoSeleccionados.Contains(x.Id)).ToListAsync();
        }
    }
}
