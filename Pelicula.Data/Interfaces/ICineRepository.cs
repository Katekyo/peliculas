﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;

namespace Pelicula.Data.Interfaces
{
    public interface ICineRepository : IGenericRepository<Cine>
    {
        Task<List<Cine>> GetCineNoSeleccionado(List<int> cinesSeleccionadosIds);
    }
}
