﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;

namespace Pelicula.Data.Interfaces
{
    public interface IGenericRepository<T> where T : EntityBaseClass
    {
        Task<IList<T>> GetAllData();
        IQueryable<T> GetAll();
        Task<T> GetById(int id);
        Task<T> Add(T item);
        Task<T> Update(T item);
        Task Remove(int id);

    }
}
